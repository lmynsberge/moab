  /*
  Range entSets = intersect(catSets,nameSets);

  std::string curStr = " ";

  char * catTag_data = new char[CATEGORY_TAG_SIZE];
  for (Range::iterator i = catSets.begin(); i != catSets.end(); ++i)
    {
      EntityHandle esId = *i;
      Range vert;

      int entID = mb->id_from_handle(esId);

      rval = mb->tag_get_data(catTag,&esId,1,catTag_data);
      std::string dataStr = std::string(catTag_data);
      std::cout << dataStr << "\n";
      rval = mb->get_entities_by_type(esId, MBVERTEX, vert);
      std::cout << "Truth: " << !(curStr.compare(dataStr)==0) << "\n";
      if( !(curStr.compare(dataStr)==0) )
        {
          bitTag_value <<= 1;
        }

      for (Range::iterator j = vert.begin(); j != vert.end(); ++j)
        {
          EntityHandle vId = *j;

          int vertID = mb->id_from_handle(vId);

          rval = mb->tag_get_data(bitTag,&vId,1,bitTag_data);
          //bitTag_value = *bitTag_data;
          if( ( (*bitTag_data % 2) == 0 ) )
            {
              *bitTag_data += 1;
            }
          std::cout << "Ent ID: " << entID << "Vert ID: " << vertID << "Bit Tag Value: " << *bitTag_data << "\n";
          rval = mb->tag_set_data(bitTag,&vId,1,bitTag_data);
        }
      curStr = dataStr;
    }
 
  char * nameTag_data = new char[NAME_TAG_SIZE];
  for (Range::iterator i = nameSets.begin(); i != nameSets.end(); ++i)
    {
      EntityHandle esId = *i;
      Range vert;

      int entID = mb->id_from_handle(esId);

      rval = mb->tag_get_data(nameTag,&esId,1,nameTag_data);
      std::string dataStr = std::string(nameTag_data);
      rval = mb->get_entities_by_type(esId, MBVERTEX, vert);
      if( !(curStr.compare(dataStr)==0) )
        {
          bitTag_value <<= 1;
        }

      for (Range::iterator j = vert.begin(); j != vert.end(); ++j)
        {
          EntityHandle vId = *j;

          int vertID = mb->id_from_handle(vId);

          rval = mb->tag_get_data(bitTag,&vId,1,bitTag_data);
          //          bitTag_value = *bitTag_data;
 
          if( ( (*bitTag_data % 2) == 0 ) )
            {
              *bitTag_data = bitTag_value + 1;
            }
          std::cout << "Ent ID: " << entID << "Vert ID: " << vertID << "Bit Tag Value: " << *bitTag_data << "\n";
          rval = mb->tag_set_data(bitTag,&vId,1,bitTag_data);
        }
      curStr = dataStr;
    }
  //////////////////////////////////////////
  /*

  char *tag_data = new char[CATEGORY_TAG_SIZE];

  Tag bitTag;
  Range verts;
  Range parents;
  long unsigned int *bitTagSpace = new long unsigned int;
  const long unsigned int default_val = 0;
  long unsigned int bitTagValue = 0;
  *bitTagSpace = bitTagValue;

  // Get all vertices
  rval = mb->get_entities_by_type(0, MBVERTEX, verts);

  // Cycle through each vertex's parents
  for (Range::iterator i = verts.begin(); i != verts.end(); ++i)
    {
      EntityHandle vId = *i; // Testing

      rval = mb->get_parent_meshsets(vId,parents,0);
      rval = mb->tag_get_handle(CATEGORY_TAG_NAME, CATEGORY_TAG_SIZE, MB_TYPE_OPAQUE, tag);
      char *tag_data = new char[CATEGORY_TAG_SIZE];
      std::string curStr;  

      int vertID = mb->id_from_handle(vId);
      std::cout << "Vertex Id: " << vertID << "\n";

      if(parents.empty())
        {
          continue;
        }

      for (Range::iterator j = parents.begin(); j != parents.end(); ++j)
        {
          EntityHandle pId = *j; // Testing

          rval = mb->tag_get_data(tag, &pId, 1, tag_data);
          curStr = std::string(tag_data);

          int parID = mb->id_from_handle(pId);
          std::cout << "Parent Id: " << parID << "\n";

          if(curStr.empty())
            {
              continue;
            }
          for (std::vector<std::string>::iterator k = vStr.begin(); k != vStr.end(); ++k)
            {
              if( (curStr.compare(*k) == 0) )
                {
                  bitTagValue += 1;
                }
              bitTagValue <<= 1;
              std::cout << "Bit Tag Value: " << bitTagValue << "\n";
            }
        }
      bitTagValue = 0;
    }

  /*





  // Get all category tag entity sets.
  char tag_data[CATEGORY_TAG_SIZE];
  rval = mb->tag_get_handle(CATEGORY_TAG_NAME, CATEGORY_TAG_SIZE, MB_TYPE_OPAQUE, tag);
  rval = mb->get_entities_by_type_and_tag(0, MBENTITYSET, &tag, NULL, 1, entsWTag, 
          Interface::UNION);

  for (Range::iterator i = entsWTag.begin(); i != entsWTag.end(); ++i)
    {
      rval = mb->tag_get_data(tag,&*i,1,tag_data);
      for(std::vector<std::string>::iterator j = vStr.begin(); j != vStr.end(); ++j)
      {

      rval = mb->get_entities_by_type(*ii,MBVERTEX,verts);


  // Assign 0 to bit tags for all vertices
  rval = mb->get_entities_by_type(0,MBVERTEX,verts);
  rval = mb->tag_get_handle(BIT_TAG_NAME,1,MB_TYPE_INTEGER, bitTag, MB_TAG_CREAT|MB_TAG_SPARSE, &default_val);

  for (Range::iterator ii = verts.begin(); ii != verts.end(); ++ii)
    {
      rval = mb->tag_set_data(bitTag,&*ii,1,bitTagSpace);

    }

  char tagName_data[NAME_TAG_SIZE];

  rval = mb->tag_get_handle(NAME_TAG_NAME, NAME_TAG_SIZE, MB_TYPE_OPAQUE, tag);

  std::cout << "Verts Size: " << verts.size() << "\n";
  */
