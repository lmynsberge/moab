#include "MBTagConventions.hpp"
#include "moab/CartVect.hpp"
#include "moab/Range.hpp"
#include "moab/Core.hpp"
#include "moab/GeomUtil.hpp"
#include "moab/Interface.hpp"
#define HIERARCHY_TAG_NAME "HIERARCHY TAG"

using namespace moab;
//------------------------------------------------------------------------------
class MOABSelectionAlgorithm
{
 public:
  Interface *Moab;
  bool BitTagVertex;
  ErrorCode LoadCatNameTags(moab::Interface *mb, std::map<std::string, std::vector<std::string> > &eMap);
  void get_category_tags(std::map<std::string, std::vector<std::string> > eMap, std::vector<std::string> &vStr);
  ErrorCode ListChildren(moab::Interface *mb, std::string parentName, std::map<std::string, std::vector<std::string> > &children);
  ErrorCode GetSelection(moab::Interface *mb, long unsigned int selMode, Range selIds, Range &results, std::vector<std::string> vStr);
  void FindOptimalEntity(moab::Interface *mb, std::vector<Range> vRange, EntityHandle &optEntity);
  EntityHandle MostCommonEnt(std::vector<EntityHandle> &vEh);
  ErrorCode BitTagVertices(moab::Interface *mb, std::vector<std::string> vStr);


  MOABSelectionAlgorithm();
  ~MOABSelectionAlgorithm();

};

